# Ultimate-Steamdeck-Startguide



## Getting started

This is a quick overview of my recommended first steps for configuring your new Steamdeck in what I think is the optimal manner.

## Lock Screen
If you wish to have a lock screen you can enable it by navigating to: Steam Button -> Settings -> Security

## Emudeck
Emudeck is the easiet way to install and run emulators on the Steamdeck. Fortunatly their documentation is quite good. Just follow these instructions: https://www.emudeck.com/#how_to_install

Once installed follow the in app instructions.

Note: When adding or removing games you need to run the rom manager app, and have steam quit from the desktop to do so, this will change the mouse and keyboard mappings temporarily.

## Decky
Decky is the premier way of installing third party plugins to your Steamdeck.
Follow the install giude here: https://github.com/SteamDeckHomebrew/decky-loader

Recomended plugins:
- ProtonDB Badges - Puts ProtonDB (Proton compatability reporting site) compatability report badges on the game page on your deck.
- AutoFlatpaks - Automatically updates flatpacks in the background when available. Helps with keeping things up to date.
- EmuDeck Hotkeys - Provides an overlay to view a cheatsheet for the EmuDeck hotkeys while in game.
- PowerTools - Advanced performance settings.
- MetaDeck - Allows tracking and showing of playtime etc for non-steam games added to the deck's launcher.
- SteamGridDB - Allows easy changing and updating of images for items in the deck launcher UI (Extremely usefull for emulation or custom apps)

## CryoUtilities
This app allows setting optimal defaults for page file and a number of other OS tweaks. 
Follow the guide here: https://github.com/CryoByte33/steam-deck-utilities
For an explination of all the tweaks see here: https://github.com/CryoByte33/steam-deck-utilities/blob/main/docs/tweak-explanation.md

## GPU VRAM Aloocation Tweak
Increase the Ram that is allocated to the GPU by default.

1. Shut down the deck
2. Hold Power + Volume up untill you hear a sound and then release to boot into the BIOS.
3. Open setup utility (bottom right option)
4. Go to the advanced tab
5. Change UMA Framebuffer from 1g to 4g
6. Press the select button (the left menu button by the thumb stick with the two squares on it) and select yes to save and exit.
7. Done, boot the deck back up, you can confirm the change took by looking in the CryoUtilities app.

(Note this wont be changed by OS Updates but may be changed by Firmware updates)

## Xbox Cloud Gaming
If you have a Gamepass subscription that gives you access to Xbox cloud gaming, follow this https://www.pcmag.com/how-to/game-pass-everywhere-how-to-install-xbox-cloud-gaming-on-steam-deck or any other guide to configure it to work smoothly on the Deck.

## Nvidia Cloud
Nvidia's cloud gaming service. There is a free teir, so no reason not to install this, good for running intensive or windows only games.
Follow this guide: https://www.pcgamesn.com/how-to-set-up-Nvidia-GeForce-Now-on-Steam-Deck

## Playstation plus Streaming
Playstation plus streaming is Sony's alternative to the above Xbox cloud. I have not tried it.
Guide: https://www.polygon.com/guides/23546431/playstation-plus-cloud-streaming-app-steam-deck-install-protonup

## Plastation Remote Play
Playstations allow you to stream your console to devices, this can be done with steamdeck althoug I have not tried it yet.
Guide: https://dashgamer.com/how-to-get-playstation-remote-play-on-steam-deck/

## Custom Boot Videos
Valve introduced official support for this on the latest update, configure the settings for this from Steam Button -> Settings -> Customazations.

Official custom videos can be purchased through steam with steam points.
Third party custom videos can be downloaded from: https://steamdeckrepo.com/

When using third party videos, download them, and then put the video files in /home/deck/.Steam/root/config/uioverrides/movies

Note: you may need to enable show hidden files in dolphin using the option in the hamburger menu on the top right of a dolphin window.